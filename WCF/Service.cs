﻿using System;
using System.ServiceModel;


namespace MediaPlayer.WCF
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		void ShowMainWindow();

		/// <summary>
		/// Call every time interval to let service alive.
		/// </summary>
		[OperationContract]
		void DoNothing();
	}



	public sealed class Service : IService
	{
		public void DoNothing() { }


		public void ShowMainWindow()
		{
			Console.WriteLine("Da nhan tin nhan !");
		}
	}
}