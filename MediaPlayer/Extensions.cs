﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Threading;
using System.Windows;


namespace MediaPlayer
{
	public static class Extensions
	{
		private static readonly string PATH = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\Media Player";
		static Extensions()
		{
			if (!Directory.Exists(PATH)) Directory.CreateDirectory(PATH);
		}


		/// <summary>
		/// <para>data == null will delete key.</para>
		/// </summary>
		public static void Write(this Application app, string key, object data, JsonSerializerSettings setting = null)
		{
			// Delete
			if (data == null)
			{
				if (app.Properties.Contains(key)) app.Properties.Remove(key);
				try
				{
					File.Delete($"{PATH}/{key}.txt");
				}
				catch (Exception) { }
				return;
			}

			// Write new or overwrite data
			app.Properties[key] = data;
			using (var writer = new StreamWriter($"{PATH}/{key}.txt"))
			{
				writer.Write(JsonConvert.SerializeObject(data, setting));
				writer.Flush();
			}
		}


		public static T Read<T>(this Application app, string key)
		{
			if (app.Properties.Contains(key)) return (T)app.Properties[key];
			using (var reader = new StreamReader($"{PATH}/{key}.txt"))
				return (T)(app.Properties[key] = JsonConvert.DeserializeObject<T>(reader.ReadToEnd()));
		}


		public static bool Contains<T>(this Application app, string key)
		{
			if (app.Properties.Contains(key)) return true;
			try { app.Read<T>(key); } catch (Exception) { return false; }
			return true;
		}


		public static void Cancel(ref CancellationTokenSource cts, bool @new = true)
		{
			if (cts == null) return;
			cts.Cancel();
			cts.Dispose();
			cts = @new ? new CancellationTokenSource() : null;
		}
	}
}
