﻿using System.Windows;


namespace MediaPlayer
{
	public partial class PopupWaiting : Window
	{
		private static PopupWaiting instance;
		public PopupWaiting()
		{
			if (instance != null) throw new System.Exception();
			instance = this;
			InitializeComponent();
		}


		private bool cancelClosing = true;
		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) => e.Cancel = cancelClosing;


		public static new void Show() => (new PopupWaiting() as Window).Show();


		public static new void Close()
		{
			instance.cancelClosing = false;
			(instance as Window).Close();
			instance = null;
		}
	}
}
