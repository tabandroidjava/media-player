﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Speech.Recognition;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;


namespace MediaPlayer
{
	public static class Database
	{
		public sealed class Folder
		{
			public string path;
			public string[] files;
			public Folder[] children;
			public Folder parent;
		}


		#region KHỞI TẠO
		public static Folder rootFolder;
		private static int totalFiles;

		/// <summary>
		/// Khởi tạo database đã xong (Chưa khởi tạo nhận dạng giọng nói).
		/// </summary>
		public static event Action initializeCompleted;


		/// <summary>
		///  Nếu có dữ liệu: rootFolder !=null.
		/// </summary>
		public static void New(string rootFolderPath, bool update)
		{
			rootFolder = null;
			totalFiles = 0;
			recognizeCompleted = false;
			result = "";

			if (!update && Application.Current.Contains<Folder>(CACHE_KEY))
			{
				rootFolder = Application.Current.Read<Folder>(CACHE_KEY);
				BFS((Folder folder) => { foreach (var child in folder.children) child.parent = folder; });
				goto COMPLETED;
			}

			if (rootFolderPath.Length != 0)
			{
				try { Path_To_Instance(rootFolderPath); }
				catch (Exception) { rootFolder = null; }
				if (rootFolder != null)
					Application.Current.Write(CACHE_KEY, rootFolder,
							   new Newtonsoft.Json.JsonSerializerSettings()
							   {
								   ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
							   });
			}

			COMPLETED:
			initializeCompleted?.Invoke();
			//Task.Run(InitializeVoiceSearch);
		}


		/// <exception cref="UnauthorizedAccessException"/>
		private static void Path_To_Instance(string rootFolderPath)
		{
			var a = new List<Folder>() { (rootFolder = new Folder() { path = rootFolderPath }) };
			var b = new List<Folder>();

			var fileList = new List<string>();
			do
			{
				foreach (var folder in a)
				{
					fileList.Clear();
					foreach (string filePath in Directory.GetFiles(folder.path))
						if (IsMediaFile(filePath)) fileList.Add(filePath);

					fileList.Sort();
					folder.files = fileList.ToArray();
					totalFiles += folder.files.Length;
					string[] children = Directory.GetDirectories(folder.path);
					folder.children = new Folder[children.Length];
					for (int i = 0; i < children.Length; ++i) b.Add(folder.children[i] = new Folder() { path = children[i], parent = folder });
				}

				var t = a;
				a = b; b = t; b.Clear();
			} while (a.Count != 0);
		}

		private const string CACHE_KEY = "CACHE";

		/// <summary>
		/// Main Thread write file.
		/// </summary>
		/// <returns><see langword="true"/>: successful</returns>
		public static bool Refresh(string rootFolderPath)
		{
			var backup = rootFolder;
			try { Path_To_Instance(rootFolderPath); }
			catch (Exception) { rootFolder = backup; return false; }
			Application.Current.Write(CACHE_KEY, rootFolder,
				new Newtonsoft.Json.JsonSerializerSettings()
				{
					ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
				});
			return true;
		}
		#endregion


		#region LỌC FILE LÀ VIDEO/ SOUND
		private static readonly List<string> MEDIA_EXTENSIONS = new List<string>()
		{
			".MP3", ".M4A", ".AAC", ".WAV", ".AMR", ".FLAC", ".MIDI", ".MID", ".MKA",
			".MP4", ".MPG", ".WMV", ".WEBM", ".FLV", ".AVI", ".3GP", ".WMA", ".MKV", ".MOV"
		};

		private static bool IsMediaFile(string filePath) => MEDIA_EXTENSIONS.Contains(Path.GetExtension(filePath).ToUpper());
		#endregion


		#region TÌM KIẾM BẰNG GÕ CHỮ
		private static readonly IReadOnlyDictionary<char, List<char>> VN_UNICODES = new Dictionary<char, List<char>>()
		{
			['A'] = new List<char>()
			{
				'Á', 'À', 'Ả', 'Ã', 'Ạ',
				'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ',
				'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ'
			},
			['D'] = new List<char>()
			{
				'Đ'
			},
			['E'] = new List<char>()
			{
				'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ',
				'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ',
			},
			['O'] = new List<char>()
			{
				'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ',
				'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ',
				'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ'
			},
			['U'] = new List<char>()
			{
				'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ',
				'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự'
			},
			['I'] = new List<char>()
			{
				'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'
			},
			['Y'] = new List<char>()
			{
				'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'
			},
		};

		public static bool IsValidKeyword(string text)
		{
			text = text.ToUpper();
			foreach (char C in text)
			{
				if (C == ' ' || ('A' <= C && C <= 'Z') || ('0' <= C && C <= '9')) continue;
				foreach (var list in VN_UNICODES.Values) if (list.Contains(C)) goto CONTINUE;
				return false;
				CONTINUE:;
			}

			return true;
		}


		private static readonly StringBuilder stringBuilder = new StringBuilder();
		public static string CreateKeyword(string text)
		{
			stringBuilder.Clear();
			foreach (char c in text) if (IsValidKeyword(c.ToString())) stringBuilder.Append(c);
			return stringBuilder.ToString();
		}


		/// <summary>
		/// Worker Thread !
		/// </summary>
		public static IReadOnlyList<string> Search(string keyword, Action<string, float> foundResult = null, CancellationToken token = default)
		{
			var result = SearchByWord(keyword, foundResult, token);
			return (token.IsCancellationRequested || result.Count != 0) ? result : SearchByCharacter(keyword, foundResult, token);
		}


		private static IReadOnlyList<string> SearchByWord(string keyword, Action<string, float> foundResult = null, CancellationToken token = default)
		{
			var keyList = ToWords(keyword);
			var a = new List<Folder>() { rootFolder };
			var b = new List<Folder>();
			var result = new List<string>();
			int fileScanned = 0;
			do
			{
				foreach (var folder in a)
				{
					for (int i = 0; i < folder.files.Length; ++i)
					{
						if (token.IsCancellationRequested) return result;
						++fileScanned;
						string filePath = folder.files[i];
						string fileName = Path.GetFileNameWithoutExtension(filePath);

						if (SourceIsResult(ToWords(fileName), keyList))
						{
							result.Add(filePath);
							foundResult?.Invoke(filePath, (float)fileScanned / totalFiles);
						}
					}
					b.AddRange(folder.children);
				}

				var t = a; a = b; b = t; b.Clear();
			} while (a.Count != 0);
			return result;
		}



		private static IReadOnlyList<string> SearchByCharacter(string keyword, Action<string, float> foundResult = null, CancellationToken token = default)
		{
			void ConvertToCompact(ref string text)
			{
				string tmp = text.ToUpper();
				text = "";
				foreach (char C in tmp)
					if (('A' <= C && C <= 'Z') || ('0' <= C && C <= '9')) text += C;
					else foreach (var kvp in VN_UNICODES)
							if (kvp.Value.Contains(C))
							{
								text += kvp.Key;
								break;
							}
			}

			ConvertToCompact(ref keyword);
			var result = new List<string>();
			var a = new List<Folder>() { rootFolder };
			var b = new List<Folder>();
			int fileScanned = 0;
			do
			{
				foreach (var folder in a)
				{
					for (int i = 0; i < folder.files.Length; ++i)
					{
						if (token.IsCancellationRequested) return result;
						string filePath = folder.files[i];
						++fileScanned;
						string fileName = Path.GetFileNameWithoutExtension(filePath);
						ConvertToCompact(ref fileName);

						if (SourceIsResult(new List<char>(fileName), keyword))
						{
							result.Add(filePath);
							foundResult?.Invoke(filePath, (float)fileScanned / totalFiles);
						}
					}
					b.AddRange(folder.children);
				}

				var t = a; a = b; b = t; b.Clear();
			} while (a.Count != 0);
			return result;
		}


		private static bool SourceIsResult<T>(List<T> source, IEnumerable<T> key)
		{
			int lastIndex = -1;
			foreach (var k in key)
				while (true)
				{
					int index = source.IndexOf(k);
					if (index < 0) return false;
					source[index] = default;
					if (index > lastIndex)
					{
						lastIndex = index; break;
					}
				}
			return true;
		}


		/// <summary>
		/// Word = {A->Z} or word={0->9}, all characters must be continous.
		/// </summary>
		private static List<string> ToWords(string text)
		{
			// Return: true= (A->Z or {Â, Ê, Ô ...}), false= (0->9), null= (other).
			bool? CheckTypeAndModify(ref char C)
			{
				if ('A' <= C && C <= 'Z') return true;
				if ('0' <= C && C <= '9') return false;
				foreach (var kvp in VN_UNICODES)
					if (kvp.Value.Contains(C))
					{
						C = kvp.Key; return true;
					}
				return null;
			}

			text = text.ToUpper();
			var _result = new List<string>();
			var word = new StringBuilder();
			int index = 0;
			while (true)
			{
				word.Clear();
				char C = text[index];
				bool? type = CheckTypeAndModify(ref C);
				if (type != null) word.Append(C);

				while (true)
				{
					if (++index == text.Length)
					{
						if (word.Length != 0) _result.Add(word.ToString());
						return _result;
					}

					C = text[index];
					if (CheckTypeAndModify(ref C) == type)
					{
						if (type != null) word.Append(C);
					}
					else
					{
						if (word.Length != 0) _result.Add(word.ToString());
						break;
					}
				}
			}
		}
		#endregion


		/// <summary>
		/// Duyệt BFS tất cả Folder
		/// </summary>
		public static void BFS(Action<Folder> result)
		{
			var a = new List<Folder>() { rootFolder };
			var b = new List<Folder>();
			do
			{
				foreach (var folder in a)
				{
					result(folder);
					b.AddRange(folder.children);
				}

				var t = a; a = b; b = t; b.Clear();
			} while (a.Count != 0);
		}


		#region CHUYỂN GIỌNG NÓI RA CHỮ
		private static SpeechRecognitionEngine voiceListener;
		private static void InitializeVoiceSearch()
		{
			voiceListener = new SpeechRecognitionEngine();
			voiceListener.SetInputToDefaultAudioDevice();
			voiceListener.SpeechRecognized += (object sender, SpeechRecognizedEventArgs e) => result = e.Result.Text;
			voiceListener.RecognizeCompleted += (object sender, RecognizeCompletedEventArgs e) => recognizeCompleted = true;

			var choices = new Choices();
			var a = new List<Folder>() { rootFolder };
			var b = new List<Folder>();
			var name = new StringBuilder();

			do
			{
				foreach (var folder in a)
				{
					foreach (string filePath in folder.files)
					{
						name.Clear();
						foreach (var w in ToWords(Path.GetFileNameWithoutExtension(filePath)))
						{
							name.Append(w);
							name.Append(' ');
						}
						choices.Add(name.ToString());
					}

					foreach (var childFolder in folder.children) b.Add(childFolder);
				}

				var t = a; a = b; b = t; b.Clear();
			} while (a.Count != 0);

			var gb = new GrammarBuilder();
			gb.Append(choices);
			gb.AppendWildcard();
			voiceListener.LoadGrammar(new Grammar(gb));
			voiceListenerInitialized?.Invoke();
		}


		/// <summary>
		/// Được gọi bằng thread khác main.
		/// </summary>
		public static event Action voiceListenerInitialized;
		private static bool recognizeCompleted;
		private static string result;

		public static async Task<string> Listen(CancellationToken token = default)
		{
			result = ""; recognizeCompleted = false;
			voiceListener.RecognizeAsync();
			while (!recognizeCompleted && !token.IsCancellationRequested) await Task.Delay(1);

			if (token.IsCancellationRequested)
			{
				result = "";
				if (!recognizeCompleted) voiceListener.RecognizeAsyncCancel();
			}
			return result;
		}
		#endregion
	}
}