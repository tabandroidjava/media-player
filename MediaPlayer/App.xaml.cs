﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;


namespace MediaPlayer
{
	public partial class App : Application
	{
		private const string PATH_KEY = "PATH";
		private static string Δpath = "";
		public static string path
		{
			get => Δpath.Length == 0 && Current.Contains<string>(PATH_KEY) ? Δpath = Current.Read<string>(PATH_KEY) : Δpath;

			set
			{
				if (Δpath == value) return;
				Δpath = value ?? throw new Exception();
				Current.Write(PATH_KEY, value);
			}
		}



		static App()
		{
			Database.initializeCompleted += () =>
			{
				PopupWaiting.Close();
				Watcher.okState = true;
				if (Database.rootFolder == null) { path = ""; Watcher.Dispose(); }
				else Watcher.New();
#if !DEBUG
				if (cmd.Length != 0 && cmd[0] == "show")
#endif
					new MainWindow().Show();
			};
		}


		private ServiceHost host;
		private static string[] cmd;
		private void Application_Startup(object sender, StartupEventArgs e)
		{
			if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
			{
				using (var client = new WCF.Generated.ServiceClient()) client.ShowMainWindow();
				Environment.Exit(0);
				return;
			}

			Service.Reset();
			PopupWaiting.Show();
			cmd = e.Args;
			Database.New(path, !Watcher.okState);
		}


		private void Application_Exit(object sender, ExitEventArgs e)
		{
			if (host == null) return;
			host.Close();
			Watcher.Dispose();
		}



		private sealed class Service : WCF.IService
		{
			public void DoNothing() { }


			public void ShowMainWindow()
			{
				if (Current.MainWindow != null) Current.MainWindow.WindowState = WindowState.Maximized;
				else new MainWindow();
				Current.MainWindow.Show();
			}


			public static void Reset()
			{
				var app = Current as App;
				try { app.host?.Close(); } catch (Exception) { }
				app.host = new ServiceHost(typeof(Service), new Uri("http://localhost:8733/MediaPlayer/Service"));
				app.host.Faulted += (s, e) => Reset();
				app.host.UnknownMessageReceived += (s, e) => Reset();
#if !DEBUG
				app.host.Closing += (s, e) => Reset();
#endif
				app.host.Open();
				KeepAlive();
			}


			#region KeepAlive
			private static CancellationTokenSource cts = new CancellationTokenSource();
			static Service()
			{
				Current.Exit += (s, e) => Extensions.Cancel(ref cts, false);
			}


			private static async void KeepAlive()
			{
				Extensions.Cancel(ref cts);
				var token = cts.Token;
				var client = new WCF.Generated.ServiceClient();
				while (true)
				{
					await Task.Delay(5 * 60 * 1000, token).ConfigureAwait(false);
					if (token.IsCancellationRequested) break;
					try { client.DoNothing(); }
					catch (Exception)
					{
						try { client.Close(); } catch (Exception) { }
						(client = new WCF.Generated.ServiceClient()).DoNothing();
					}
				}

				try { client.Close(); } catch (Exception) { }
			}
			#endregion
		}


		private static readonly string LOG_PATH = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MediaPlayer";
		private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			if (!Directory.Exists(LOG_PATH)) Directory.CreateDirectory(LOG_PATH);
			using (var stream = new FileStream($@"{LOG_PATH}\ERROR.txt", FileMode.Create))
			using (var writer = new StreamWriter(stream)) writer.WriteLine(e.Exception);

			host?.Close();
			Watcher.okState = false;
			MessageBox.Show($"{e.Exception}\n.See log file at {LOG_PATH}\\ERROR.txt");
			e.Handled = true;
			Environment.Exit(1);
		}
	}
}
