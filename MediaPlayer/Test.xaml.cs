﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows;


namespace MediaPlayer
{
	public partial class Test : Window
	{
		public Test()
		{
			InitializeComponent();
		}


		private ServiceHost host;
		private void Host(object sender, RoutedEventArgs arg)
		{
			host = new ServiceHost(typeof(Service), new Uri("http://localhost:8733/MediaPlayer/Service"));
			host.Open();
			MessageBox.Show("Host open");
		}


		private void Client(object sender, RoutedEventArgs arg)
		{
			using (var client = new WCF.Generated.ServiceClient()) client.ShowMainWindow();
		}


		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			host?.Close();
		}
	}



	public class Service : WCF.IService
	{
		public void DoNothing()
		{
			throw new System.NotImplementedException();
		}


		public void ShowMainWindow()
		{
			MessageBox.Show("Show main window !");
		}
	}
}