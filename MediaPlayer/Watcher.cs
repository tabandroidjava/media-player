﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;


namespace MediaPlayer
{
	/// <summary>
	/// Đăng kí quan sát các folder và thực hiện cập nhật rootFolder và database.
	/// </summary>
	public static class Watcher
	{
		private static readonly List<FileSystemWatcher> watchers = new List<FileSystemWatcher>();
		private static readonly ConcurrentQueue<WatcherChangeTypes> queue = new ConcurrentQueue<WatcherChangeTypes>();

		#region << okState >>
		private const string OKSTATE_KEY = "OKSTATE";
		private static bool? ΔokState;

		/// <summary>
		/// Trạng thái dữ liệu hiện tại/lần chạy trước đó có toàn vẹn hay không ?
		/// <para><see langword="true"/> là toàn vẹn.</para>
		/// </summary>
		public static bool okState
		{
			get => ΔokState != null ? ΔokState.Value : App.Current.Contains<bool>(OKSTATE_KEY);

			set
			{
				if (ΔokState == value) return;
				App.Current.Write(OKSTATE_KEY, (ΔokState = value) == true ? true : (bool?)null);
			}
		}
		#endregion


		public static void New()
		{
			watchers.Clear();
			while (!queue.IsEmpty) queue.TryDequeue(out _);
			ΔokState = null;
			token = (cancelSource = new CancellationTokenSource()).Token;
			Register();
		}


		private static bool Register(string path)
		{
			FileSystemWatcher watcher = null;
			try
			{
				watcher = new FileSystemWatcher(path);
			}
			catch (Exception)
			{
				watcher?.Dispose();
				return false;
			}

			watcher.IncludeSubdirectories = false;
			watcher.Created += Watcher_Created;
			watcher.Deleted += Watcher_Deleted;
			watcher.Changed += Watcher_Changed;
			watcher.Renamed += Watcher_Renamed;
			watcher.Error += Watcher_Error;
			watchers.Add(watcher);
			watcher.EnableRaisingEvents = true;
			return true;
		}


		private static async void Register()
		{
			var unregs = new List<Database.Folder>();
			Database.BFS(unregs.Add);
			var ready = new Dictionary<string, bool>()
			{
				[@"C:\"] = false,
				[@"D:\"] = false,
				[@"E:\"] = false
			};

			while (true)
			{
				DriveInfo[] drives;
				try
				{
					drives = DriveInfo.GetDrives();
				}
				catch (Exception) { await Task.Delay(1); if (token.IsCancellationRequested) break; continue; }

				foreach (var drive in drives)
					if (ready.ContainsKey(drive.Name) && !ready[drive.Name] && drive.IsReady)
					{
						ready[drive.Name] = drive.IsReady;
						var t = new List<Database.Folder>();
						foreach (var folder in unregs) if (!Register(folder.path)) t.Add(folder);
						unregs = t;
					}

				if (ready[@"C:\"] && ready[@"D:\"] && ready[@"E:\"]) break;
				await Task.Delay(1);
				if (token.IsCancellationRequested) break;
			}
		}


		#region << Xóa đối tượng và hủy các task của đối tượng >>
		private static CancellationTokenSource cancelSource;
		private static CancellationToken token;

		public static void Dispose()
		{
			Extensions.Cancel(ref cancelSource, false);
			foreach (var w in watchers)
			{
				w.EnableRaisingEvents = false;
				w.Dispose();
			}
		}
		#endregion


		#region << Các sự kiện FileSystem thay đổi >>
		private static void Watcher_Error(object sender, ErrorEventArgs e)
		{
			var w = sender as FileSystemWatcher;
			string path = w.Path;
			w.Dispose();
			watchers.Remove(w);
			if (Directory.Exists(path)) Register(path);
			queue.Enqueue(WatcherChangeTypes.All);
			App.Current.Dispatcher.Invoke(() =>
			{
				okState = false;
				if (App.Current.MainWindow?.IsActive == true) CheckReset();
			});
		}


		private static void Watcher_Renamed(object sender, RenamedEventArgs e)
		{
			queue.Enqueue(WatcherChangeTypes.Renamed);
			App.Current.Dispatcher.Invoke(() =>
			{
				okState = false;
				if (App.Current.MainWindow?.IsActive == true) CheckReset();
			});
		}


		private static void Watcher_Changed(object sender, FileSystemEventArgs e)
		{
			queue.Enqueue(WatcherChangeTypes.Changed);
			App.Current.Dispatcher.Invoke(() =>
			{
				okState = false;
				if (App.Current.MainWindow?.IsActive == true) CheckReset();
			});
		}


		private static void Watcher_Deleted(object sender, FileSystemEventArgs e)
		{
			queue.Enqueue(WatcherChangeTypes.Deleted);
			App.Current.Dispatcher.Invoke(() =>
			{
				okState = false;
				if (App.Current.MainWindow?.IsActive == true) CheckReset();
			});
		}


		private static void Watcher_Created(object sender, FileSystemEventArgs e)
		{
			queue.Enqueue(WatcherChangeTypes.Created);
			App.Current.Dispatcher.Invoke(() =>
			{
				okState = false;
				if (App.Current.MainWindow?.IsActive == true) CheckReset();
			});
		}
		#endregion


		/// <returns><see langword="true"/>: có Reset</returns>
		public static bool CheckReset()
		{
			if (token.IsCancellationRequested) return false;
			int count = queue.Count;
			if (count != 0)
			{
				for (int i = 0; i < count; ++i) queue.TryDequeue(out _);
				(App.Current.MainWindow as MainWindow).Close();
				PopupWaiting.Show();
				if (!Database.Refresh(App.path)) throw new Exception();
				okState = true;
				PopupWaiting.Close();
				new MainWindow().Show();
				return true;
			}
			return false;
		}
	}
}